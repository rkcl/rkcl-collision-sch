# [](https://gite.lirmm.fr/rkcl/rkcl-collision-sch/compare/v2.1.0...v) (2022-07-21)



# [2.1.0](https://gite.lirmm.fr/rkcl/rkcl-collision-sch/compare/v2.0.0...v2.1.0) (2022-03-31)


### Features

* added utility fcts ([1c7c6f8](https://gite.lirmm.fr/rkcl/rkcl-collision-sch/commits/1c7c6f865ee630bddbdf86c0912e4dd21c2a564d))
* allow to update distance eval witout generating constraints ([1a65394](https://gite.lirmm.fr/rkcl/rkcl-collision-sch/commits/1a65394d5bc747ce04c6b1ffc6f9dc04b0b47c8b))



# [2.0.0](https://gite.lirmm.fr/rkcl/rkcl-collision-sch/compare/v1.1.0...v2.0.0) (2021-09-30)


### Bug Fixes

* updated virtual fct to match base class ([6e1eeda](https://gite.lirmm.fr/rkcl/rkcl-collision-sch/commits/6e1eedad23117c9bf202075ad112e65e8445547d))


### Features

* use conventional commits ([86513ed](https://gite.lirmm.fr/rkcl/rkcl-collision-sch/commits/86513ed46480113682622c73e1042d084bd0174a))



# [1.1.0](https://gite.lirmm.fr/rkcl/rkcl-collision-sch/compare/v1.0.3...v1.1.0) (2020-10-13)



## [1.0.3](https://gite.lirmm.fr/rkcl/rkcl-collision-sch/compare/v1.0.2...v1.0.3) (2020-03-23)



## [1.0.2](https://gite.lirmm.fr/rkcl/rkcl-collision-sch/compare/v1.0.1...v1.0.2) (2020-03-05)



## 1.0.1 (2020-02-20)




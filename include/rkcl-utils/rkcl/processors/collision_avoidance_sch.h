/**
 * @file collision_avoidance_sch.h
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a collision avoidance processor based on sch-core
 * @date 30-01-2020
 * License: CeCILL
 */

#pragma once

#include <rkcl/core.h>

#include <sch/CD/CD_Pair.h>

namespace rkcl
{
/**
 * @brief Class for collision avoidance using sch-core, callable
 *
 */
class CollisionAvoidanceSCH : virtual public CollisionAvoidance
{
public:
    /**
	* @brief Structure holding the data for a self collision evaluation
	*
	*/
    struct SelfCollisionPair
    {
        size_t robot_collision_object1_index;             //!< index of the first robot collision object
        size_t robot_collision_object2_index;             //!< index of the second robot collision object
        std::shared_ptr<sch::CD_Pair> sch_collision_pair; //!<pair of sch objects used to evaluate the distance
    };

    /**
	 * @brief Structure holding the data for a collision evaluation with an object of the world
	 *
	 */
    struct WorldCollisionPair
    {
        size_t robot_collision_object_index;              //!< index of the robot collision object
        size_t world_collision_object_index;              //!< index of the world collision object
        std::shared_ptr<sch::CD_Pair> sch_collision_pair; //!< pair of sch objects used to evaluate the distance
    };

    /**
	 * @brief Construct a new SCH Collision Avoidance object
	 * @param robot reference to the shared robot
	 * @param fk pointer to the FK object
	 */
    CollisionAvoidanceSCH(Robot& robot, ForwardKinematicsPtr fk);
    /**
	 * @brief Construct a new SCH Collision Avoidance object using a YAML configuration file
	 * The configuration file is passed to the base class constructor
	 * @param robot reference to the shared robot
	 * @param fk pointer to the forward kinematic object
	 * @param configuration The YAML node containing the point wrench estimator parameters
	 */
    CollisionAvoidanceSCH(Robot& robot, ForwardKinematicsPtr fk, const YAML::Node& configuration);

    /**
	 * @brief Execute the collision avoidance process
	 * @return true on success, false otherwise
	 */
    bool process() override;
    /**
	 * @brief Initialize the collision avoidance data (called once at the beginning of the program)
	 * @return true on success
	 */
    void init() override;
    void reset() override;

    /**
     * @brief Disable collisions between a link (identified by 'link') and a set of other links (identified by 'other_links')
     * @param disable_collisions The YAML node containing a list of elements
     */
    void disableCollisions(const YAML::Node& disable_collisions);

    /**
	 * @brief Create SCH objects from robot collision objects and add them to the member vector
	 */
    void createSCHRobotCollisionObjects();
    /**
	 * @brief  Create SCH objects from world collision objects and add them to the member vector
	 */
    void createSCHWorldCollisionObjects();
    /**
	 * @brief Create SelfCollisionPair for pairs of robot's links which can potentially collide and add them to the member vector
	 */
    void createSCHObjectSelfCollisionPairs();
    /**
	 * @brief Create SelfCollisionPair for pairs of robot/world objects which can potentially collide and add them to the member vector
	 */
    void createSCHObjectWorldCollisionPairs();

    /**
	 * @brief Update the pose of SCH objects from the ones newly stored in the robot collision objects
	 */
    void updateRobotObjectsPose();
    /**
	 * @brief Update the pose of SCH objects from the ones newly stored in the world collision objects
	 *
	 */
    void updateWorldObjectsPose();
    /**
	 * @brief Perform the distance evaluation between pairs of robot's links which can potentially collide
	 * @return true on success, false otherwise
	 */
    bool evalSelfCollisionDistances();
    /**
	 * @brief Perform the distance evaluation between pairs of robot/world objects which can potentially collide
	 * @return true on success, false otherwise
	 */
    bool evalWorldCollisionDistances();
    /**
	 * @brief Evaluate the velocity damper term between two witness points
	 * @param point1 coordinates of the first point
	 * @param point2 coordinates of the second point
	 * @return the value of the velocity damper
	 */
    double computeVelocityDamper(const Eigen::Vector3d& point1, const Eigen::Vector3d& point2);
    /**
	 * @brief Compute the velocity damper for each pair of objects and set it as constraints to
	 * the different robot joint groups
	 */
    void setRobotVelocityDamper();
    /**
	 * @brief Clear the collision evaluation data
	 */
    void clearData();
    /**
	 * @brief Compute a repulsive action between near objects
	 * See "A Depth Space Approach to Human-Robot Collision Avoidance" for more details on the approach
	 */
    void computeRepulsiveTwists();

    bool updateDistanceEval();

private:
    std::vector<std::shared_ptr<sch::S_Object>> sch_robot_collision_objects_; //!< Vector of SCH objects associated to robot collision objects
    std::vector<std::shared_ptr<sch::S_Object>> sch_world_collision_objects_; //!< Vector of SCH objects associated to world collision objects
    std::vector<SelfCollisionPair> self_collision_pairs_;                     //!<vector of potentially colliding pair of robot/robot objects
    std::vector<WorldCollisionPair> world_collision_pairs_;                   //!<vector of potentially colliding pair of robot/world objects

    // TEMP
    double total_time{0};
    size_t nb_calls{0};
};
} // namespace rkcl
